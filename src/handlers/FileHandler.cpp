#include "jonmiller131313/handlers/FileHandler.hpp"

namespace jonmiller131313::logging {

FileHandler::FileHandler(FileHandler &&mv) noexcept :
        StreamHandler(std::move(mv)),
        filePath(std::move(mv.filePath)),
        overwrite(mv.overwrite) {
}

FileHandler& FileHandler::operator=(FileHandler &&mv) noexcept {
    filePath = std::move(mv.filePath);
    overwrite = mv.overwrite;
    StreamHandler::operator=(std::move(mv));

    return *this;
}

auto FileHandler::getPath() const -> const fs::path& {
    return filePath;
}

void FileHandler::open() {
    auto modeFlags = std::ios_base::out;
    if(overwrite)
        modeFlags |= std::ios_base::trunc;

    stream->open(filePath, modeFlags);
    std::unitbuf(*stream);
}

void FileHandler::close() {
    stream->close();
}

}
