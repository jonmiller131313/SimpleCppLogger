/**
 * @file JsonFormatter.hpp
 * @brief A class for formatting log entries into json objects.
 * @copyright LGPLv3+ (c) 2019 Jonathan Miller
 * @code{.md}
 ***********************************************************************
 * This file is part of SimpleCppLogger:
 *    https://gitlab.com/jonmiller131313/SimpleCppLogger
 *
 *    SimpleCppLogger is free software: you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public License
 *    as published by the Free Software Foundation, either version 3 of
 *    the License, or (at your option) any later version.
 *
 *    SimpleCppLogger is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with SimpleCppLogger.
 *    If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************
 * @endcode
 */
#ifndef JONMILLER131313_JSONFORMATTER_HPP
#define JONMILLER131313_JSONFORMATTER_HPP

#include <string>

#include "Formatter.hpp"
#include "jonmiller131313/logging_utils.hpp"

namespace jonmiller131313::logging {

    /// Class for formatting log entries into JSON.
    class JsonFormatter : public Formatter {
        public:
            /// The constructor. Initializes the timestamp format to ISO-8601.
            JsonFormatter();

            JsonFormatter(const JsonFormatter &cpy) = default;

            JsonFormatter(JsonFormatter &&mv) = default;

            JsonFormatter& operator=(const JsonFormatter &cpy) = default;

            JsonFormatter& operator=(JsonFormatter &&mv) = default;

            [[nodiscard]] auto format(const LogEntry &entry) const -> std::string override;

    };
}


#endif

