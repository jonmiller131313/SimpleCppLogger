/**
 * @file StreamHandler.hpp
 * @brief Base stream handler class.
 * @copyright LGPLv3+ (c) 2019 Jonathan Miller
 * @code{.md}
 ***********************************************************************
 * This file is part of SimpleCppLogger:
 *    https://gitlab.com/jonmiller131313/SimpleCppLogger
 *
 *    SimpleCppLogger is free software: you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public License
 *    as published by the Free Software Foundation, either version 3 of
 *    the License, or (at your option) any later version.
 *
 *    SimpleCppLogger is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with SimpleCppLogger.
 *    If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************
 * @endcode
 */
#ifndef JONMILLER131313_STREAMHANDLER_HPP
#define JONMILLER131313_STREAMHANDLER_HPP

#include <iostream>
#include <functional>
#include <string>
#include <utility>

#include "jonmiller131313/formatters/Formatter.hpp"
#include "jonmiller131313/handlers/Handler.hpp"
#include "jonmiller131313/logging_utils.hpp"

namespace jonmiller131313::logging {

    /// A class to manage streams for handling log entries.
    template<class StreamClass,
             class C = std::enable_if_t<std::is_base_of_v<std::ostream, StreamClass>>>
    class StreamHandler : public Handler {
        public:
            /**
             * @brief The constructor.
             *
             * @param stream The stream to use, takes ownership of it unless it's `std::cout` or `std::cerr`.
             * @param formatter The entry formatter.
             */
            template<class F = StringFormatter>
            explicit StreamHandler(StreamClass &&s, F &&formatter = StringFormatter(), log_level level = NOT_SET) :
                    Handler(std::forward<F>(formatter), level),
                    stream(std::make_unique<StreamClass>(std::move(s))) {}

            /// No duplicate handlers.
            StreamHandler(const StreamHandler &cpy) = delete;

            /// The move constructor.
            StreamHandler(StreamHandler &&mv) noexcept = default;

            /// The destructor.
            ~StreamHandler() override = default;

            /// No copy assignment operator.
            StreamHandler& operator=(const StreamHandler &cpy) = delete;

            /// The move assignment operator.
            StreamHandler& operator=(StreamHandler &&mv) noexcept = default;

            void handleEntry(const LogEntry &entry) const override {
                const auto formattedEntry = formatter->format(entry);

                stream->write(formattedEntry.c_str(), formattedEntry.size());
            }

            /// Flushes the stream.
            void flush() const override {
                stream->flush();
            }

        protected:
            /// Our owned output stream
            std::unique_ptr<StreamClass> stream;
    };

    template<>
    class StreamHandler<std::ostream> : public Handler {
        public:
            template<class F>
            explicit StreamHandler(std::ostream &s, F &&formatter = StringFormatter(), log_level level = NOT_SET) :
                Handler(std::forward<F>(formatter), level),
                stream(s) {}

            StreamHandler(const StreamHandler &cpy) = delete;

            StreamHandler(StreamHandler &&mv) = delete;

            StreamHandler& operator=(const StreamHandler &cpy) = delete;

            StreamHandler& operator=(StreamHandler &&mv) = delete;

            void handleEntry(const LogEntry &entry) const override {
                const auto formattedEntry = formatter->format(entry);

                stream.write(formattedEntry.c_str(), static_cast<std::streamsize>(formattedEntry.size()));
            }

            /// Flushes the stream.
            void flush() const override{
                stream.flush();
            }

        protected:
            std::ostream &stream;
    };

    /**
     * @brief Generates a stream handler that outputs to standard output.
     *
     * @param formatter The formatter.
     * @param level The log level.
     *
     * @return A handler to standard out.
     *
     */
    template<class F = StringFormatter>
    auto StdOutHandler(F &&formatter = StringFormatter(), log_level level = NOT_SET) {
        return std::make_unique<StreamHandler<decltype(std::cout)>>(std::cout, std::forward<F>(formatter), level);
    }

    /**
     * @brief Generates a stream handler that outputs to standard error.
     *
     * @param level The log level.
     * @param formatter The formatter.
     *
     * @return A handler to standard error.
     *
     */
    template<class F = StringFormatter>
    auto StdErrHandler(F &&formatter = StringFormatter(), log_level level = NOT_SET) {
        return std::make_unique<StreamHandler<decltype(std::cerr)>>(std::cerr, std::forward<F>(formatter), level);
    }

}

#endif
