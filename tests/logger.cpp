#include <gtest/gtest.h>

#include <string>

#include "jonmiller131313/logging.hpp"

using namespace jonmiller131313::logging;
namespace gtest = ::testing::internal;

using namespace std::string_literals;

class LoggerTest : public ::testing::Test {
    protected:
        ~LoggerTest() override {
            Logger::clearLoggers();
        }
};

void inline captureOutput() {
    gtest::CaptureStdout();
    gtest::CaptureStderr();
}


TEST_F(LoggerTest, GetLogger) {
    auto &logger = Logger::getLogger("gtest");
    auto &otherLogger = Logger::getLogger("gtest");

    EXPECT_EQ(&logger, &otherLogger);
}

TEST_F(LoggerTest, Handlers) {
    auto &logger = Logger::getLogger("gtest");
    const auto f = StringFormatter("%M");
    const auto logMessage = "This is a unit test."s;

    logger.addHandler(StdOutHandler(f));
    logger.addHandler(StdErrHandler(f));

    captureOutput();

    logger.verbose(logMessage);

    const auto stdoutOutput = gtest::GetCapturedStdout();
    const auto stderrOutput = gtest::GetCapturedStderr();

    EXPECT_EQ(stdoutOutput, stderrOutput);
    EXPECT_EQ(stdoutOutput, logMessage + '\n');
}

TEST_F(LoggerTest, DefaultHandler) {
    auto &logger = Logger::getLogger("gtest");
    const auto logMessage = "This is a unit test."s;

    captureOutput();

    logger.debug(logMessage);

    auto stdoutOutput = gtest::GetCapturedStdout();
    auto stderrOutput = gtest::GetCapturedStderr();

    EXPECT_EQ(stderrOutput.length(), 0);
    EXPECT_EQ(stdoutOutput, "DEBUG:gtest:" + logMessage + '\n');
}

TEST_F(LoggerTest, BasicConfig) {
    auto &logger = Logger::getLogger("gtest");
    const auto logMessage = "This is a unit test."s;
    logger.basicConfig("%M");

    captureOutput();
    logger.debug(logMessage);

    const auto stdoutOutput = gtest::GetCapturedStdout();
    const auto stderrOutput = gtest::GetCapturedStderr();

    EXPECT_EQ(stderrOutput.length(), 0);
    EXPECT_EQ(stdoutOutput, logMessage + '\n');
}

TEST_F(LoggerTest, GlobalConfig) {
    Logger::globalConfig("%M");
    auto &logger = Logger::getLogger("gtest");
    const auto logMessage = "This is a unit test."s;

    captureOutput();
    logger.debug(logMessage);

    const auto stdoutOutput = gtest::GetCapturedStdout();
    const auto stderrOutput = gtest::GetCapturedStderr();

    EXPECT_EQ(stderrOutput.length(), 0);
    EXPECT_EQ(stdoutOutput, logMessage + '\n');
}

TEST_F(LoggerTest, DefaultConfig) {
    auto &logger = Logger::getLogger("gtest");
    const auto LOG_MESSAGE = "This is a unit test."s;

    captureOutput();
    logger.debug(LOG_MESSAGE);

    const auto stdoutOutput = gtest::GetCapturedStdout();
    const auto stderrOutput = gtest::GetCapturedStderr();

    EXPECT_EQ(stderrOutput.length(), 0);
    EXPECT_EQ(stdoutOutput, "DEBUG:gtest:" + LOG_MESSAGE + '\n');
}

TEST_F(LoggerTest, RootLogger) {
    auto &logger = Logger::r();
    const auto LOG_MESSAGE = "This is a unit test."s;

    captureOutput();
    logger.debug(LOG_MESSAGE);

    const auto stdoutOutput = gtest::GetCapturedStdout();
    const auto stderrOutput = gtest::GetCapturedStderr();

    EXPECT_EQ(stderrOutput.length(), 0);
    EXPECT_EQ(stdoutOutput, "DEBUG:root:" + LOG_MESSAGE + '\n');
}

TEST_F(LoggerTest, GettersAndSetters) {
    auto &logger = Logger::getLogger("gtest");

    EXPECT_EQ(logger.getName(), "gtest"s);
    EXPECT_EQ(logger.getLevel(), NOT_SET);

    logger.setLevel(INFO);
    EXPECT_EQ(logger.getLevel(), INFO);
}

TEST_F(LoggerTest, FilterLevels) {
    auto &logger = Logger::getLogger("gtest", INFO);
    auto f = StringFormatter("%M");

    auto handlers = std::vector<std::unique_ptr<Handler>>();
    handlers.emplace_back(StdOutHandler(f));
    handlers.emplace_back(StdErrHandler(f, WARNING));
    logger.addHandlers(std::move(handlers));

    captureOutput();
    logger.verbose("Debug");
    auto stdoutOutput = gtest::GetCapturedStdout();
    auto stderrOutput = gtest::GetCapturedStderr();
    EXPECT_EQ(stdoutOutput, stderrOutput);
    ASSERT_EQ(stdoutOutput, "");

    captureOutput();
    logger.info("Information");
    stdoutOutput = gtest::GetCapturedStdout();
    stderrOutput = gtest::GetCapturedStderr();
    EXPECT_EQ(stdoutOutput, "Information\n");
    ASSERT_EQ(stderrOutput, "");

    captureOutput();
    logger.error("Error");
    stdoutOutput = gtest::GetCapturedStdout();
    stderrOutput = gtest::GetCapturedStderr();
    EXPECT_EQ(stdoutOutput, stderrOutput);
    EXPECT_EQ(stdoutOutput, "Error\n");
}
