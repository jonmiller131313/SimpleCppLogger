/**
 * @file Formatter.hpp
 * @brief Log entry formatting base class.
 * @copyright LGPLv3+ (c) 2019 Jonathan Miller
 * @code{.md}
 ***********************************************************************
 * This file is part of SimpleCppLogger:
 *    https://gitlab.com/jonmiller131313/SimpleCppLogger
 *
 *    SimpleCppLogger is free software: you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public License
 *    as published by the Free Software Foundation, either version 3 of
 *    the License, or (at your option) any later version.
 *
 *    SimpleCppLogger is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with SimpleCppLogger.
 *    If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************
 * @endcode
 */
#ifndef JONMILLER131313_FORMATTER_HPP
#define JONMILLER131313_FORMATTER_HPP

#include <string>

#include <ctime>

namespace jonmiller131313::logging {

    class LogEntry;

    /// Base class for formatting log entries.
    class Formatter {
        public:
            /**
             * @brief The constructor.
             *
             * @param dateTimeFormat The format of the date and time values.
             *
             * @see setDateTimeFormat()
             */
            explicit Formatter(std::string dateTimeFormat = "%F %X");

            /// The copy constructor
            Formatter(const Formatter &cpy) = default;

            /// The move constructor.
            Formatter(Formatter &&mv) = default;

            /// The destructor.
            virtual ~Formatter() = default;

            /// The copy assignment operator.
            Formatter& operator=(const Formatter &cpy) = default;

            /// The move assignment operator.
            Formatter& operator=(Formatter &&mv) = default;

            /**
             * @brief Sets the date and time format.
             *
             * @param newDateTimeFormat The new format. The string is passed into `std::put_time()` which uses
             *                          `strftime()` so the formatting tags are the same.
             */
            void setDateTimeFormat(const std::string& newDateTimeFormat);

            /**
             * @brief Gets the date and time format.
             *
             * @return The date and time format.
             */
            [[nodiscard]] auto getDateTimeFormat() const -> const std::string&;

            /**
             * @brief Gets a formatted string of the date and time.
             *
             * @param timestamp The time to use for formatting.
             *
             * @return The formatted date and time.
             */
            [[nodiscard]] auto getFormattedDateTime(std::time_t timestamp) const -> std::string;

            /**
             * @brief Formats the whole log entry.
             *
             * Should be implemented in extended classes.
             *
             * @param entry The entry to format.
             *
             * @return The formatted log entry.
             */
            [[nodiscard]] virtual auto format(const LogEntry &entry) const -> std::string = 0;

        protected:
            /// The date and time format.
            std::string dateTimeFormat;
    };
}


#endif

