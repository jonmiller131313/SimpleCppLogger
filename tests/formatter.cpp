#include <ctime>

#include <gtest/gtest.h>

#include "jonmiller131313/logging.hpp"

using namespace jonmiller131313::logging;

const static std::time_t timestamp = 15000;

class StringFormatterTest : public ::testing::Test {
    protected:
        const LogEntry logEntry;

        StringFormatterTest() :
                logEntry("This is a unit test.",
                         DEBUG,
                         "formatter.cpp",
                         "unit_test",
                         25,
                         "logger_name",
                         timestamp,
                         "a.out") {}
};

class JsonFormatterTest : public StringFormatterTest {};


TEST_F(StringFormatterTest, Default) {
    auto formatter = StringFormatter();
    auto fmtBuff = std::array<char, 1024>();
    strftime(fmtBuff.data(), fmtBuff.size(), "%F %X", localtime(&logEntry.logTime));

    EXPECT_EQ(formatter.getFormat(), "%T:%N:%M");
    EXPECT_EQ(formatter.getDateTimeFormat(), "%F %X");

    EXPECT_EQ(formatter.format(logEntry), "DEBUG:logger_name:This is a unit test.\n");
    EXPECT_STREQ(fmtBuff.data(), formatter.getFormattedDateTime(logEntry.logTime).c_str());
}

TEST_F(StringFormatterTest, CustomFormat) {
    const auto FORMAT = "[%D %15F:%l] %T - %f: ";
    const auto TIME_FORMAT = "%FT%X";
    auto formatter = StringFormatter(FORMAT, TIME_FORMAT);
    auto fmtBuff = std::array<char, 1024>();
    strftime(fmtBuff.data(), fmtBuff.size(), TIME_FORMAT, localtime(&logEntry.logTime));

    EXPECT_EQ(formatter.getFormat(), std::string(FORMAT) + "%M");
    EXPECT_STREQ(formatter.getDateTimeFormat().c_str(), TIME_FORMAT);

    EXPECT_EQ(formatter.format(logEntry), std::string("[") + fmtBuff.data() + "   formatter.cpp:25] DEBUG - unit_test(): This is a unit test.\n");
}

TEST_F(JsonFormatterTest, JsonTest) {
    auto formatter = JsonFormatter();
    auto fmtBuff = std::array<char, 1024>();
    strftime(fmtBuff.data(), fmtBuff.size(), "%FT%X", localtime(&logEntry.logTime));
    const auto jsonOutput = std::string("{\"timestamp\":\"") + fmtBuff.data() + "\",\"log_level\":\"DEBUG\",\"file\":\"formatter.cpp\",\"function\":\"unit_test()\",\"line\":25,\"logger_name\":\"logger_name\",\"executing_file\":\"a.out\",\"pid\":" + std::to_string(getpid()) +",\"message\":\"This is a unit test.\"}\n";

    EXPECT_EQ(formatter.getDateTimeFormat(), "%FT%X");
    EXPECT_EQ(formatter.format(logEntry), jsonOutput);
}
