/**
 * @file Handler.hpp
 * @brief Base handler class for log entries.
 * @copyright LGPLv3+ (c) 2019 Jonathan Miller
 * @code{.md}
 ***********************************************************************
 * This file is part of SimpleCppLogger:
 *    https://gitlab.com/jonmiller131313/SimpleCppLogger
 *
 *    SimpleCppLogger is free software: you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public License
 *    as published by the Free Software Foundation, either version 3 of
 *    the License, or (at your option) any later version.
 *
 *    SimpleCppLogger is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with SimpleCppLogger.
 *    If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************
 * @endcode
 */
#ifndef JONMILLER131313_HANDLER_HPP
#define JONMILLER131313_HANDLER_HPP

#include <algorithm>
#include <memory>
#include <utility>
#include <vector>

#include "jonmiller131313/formatters/Formatter.hpp"
#include "jonmiller131313/formatters/StringFormatter.hpp"
#include "jonmiller131313/logging_utils.hpp"

namespace jonmiller131313::logging {

    /// The base class for handling log entries.
    class Handler {
        public:
            /**
             * @brief The constructor.
             *
             * @param formatter The formatter to use to format entries. Defaults to a string formatter.
             * @param level The log level filter.
             */
            template<class F = StringFormatter,
                     typename std::enable_if<std::is_base_of<Formatter, F>::value>* = nullptr>
            explicit Handler(const F &formatter = StringFormatter(), log_level level = NOT_SET) noexcept:
                    formatter(std::make_unique<F>(formatter)),
                    logLevel(level) {}

            /**
             * @brief C-String StringFormatter initializer.
             *
             * @param fmt The hardcoded format string to initialize this handler
             *            with.
             * @param level The log level filter.
             */
            explicit Handler(const char *fmt, log_level level = NOT_SET);

            /// Cannot have duplicate handler objects.
            Handler(const Handler &cpy) = delete;

            /// The move constructor.
            Handler(Handler &&mv) = default;

            /// The destructor.
            virtual ~Handler() = default;

            /// No copy assignment operator.
            Handler& operator=(const Handler &cpy) = delete;

            /// The move assignment operator.
            Handler& operator=(Handler &&mv) = default;

            /**
             * @brief Sets the level filter for this handler.
             *
             * If entries handled by this object are lower than its level then
             * the entry is ignored.
             *
             * @param newLevel The new level.
             */
            void setLevel(log_level newLevel) noexcept;

            /**
             * @brief Gets the level filter.
             *
             * @return The level.
             */
            [[nodiscard]] auto getLevel() const noexcept -> log_level;

            /**
             * @brief Sets the formatter for this handler.
             *
             * @param f The formatter to format log entries when they are handled.
             */
            template<class F>
            void setFormatter(F &&f) {
                this->formatter = std::make_shared(std::forward(f));
            }

            /**
             * @brief Gets the formatter.
             *
             * @return A smart pointer to the formatter.
             */
            [[nodiscard]] auto getFormatter() const -> const std::unique_ptr<Formatter>&;

            /**
             * @brief Function to handle log entries, called from the Logger
             *        class.
             *
             * Checks if the entry's level is lower than the handler's, if so it
             * is ignored, otherwise call's @ref emit().
             *
             * @param entry The log entry to handle.
             *
             * @see emit()
             */
            void handle(const LogEntry &entry) const;

            /**
             * @brief The function that actually "handles" the log entry.
             *
             * Should be implemented by extended classes.
             *
             * @param entry The log entry to have action on.
             */
            virtual void handleEntry(const LogEntry &entry) const = 0;

            /**
             * @brief Initialization function for the handler.
             *
             * By default does nothing.
             */
            virtual void open();

            /**
             * @brief Flushes or updates the handler.
             *
             * By default does nothing.
             */
            virtual void flush() const;

            /**
             * @brief Cleans up the handler.
             *
             * By default does nothing.
             */
            virtual void close();

        protected:
            /// The formatter that formats each log entry when handled.
            std::unique_ptr<Formatter> formatter;

            /// The log level filter.
            log_level logLevel;
    };

}

#endif
