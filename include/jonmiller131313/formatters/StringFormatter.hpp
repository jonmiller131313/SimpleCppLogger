/**
 * @file StringFormatter.hpp
 * @brief A class for formatting log entries into basic strings.
 * @copyright LGPLv3+ (c) 2019 Jonathan Miller
 * @code{.md}
 ***********************************************************************
 * This file is part of SimpleCppLogger:
 *    https://gitlab.com/jonmiller131313/SimpleCppLogger
 *
 *    SimpleCppLogger is free software: you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public License
 *    as published by the Free Software Foundation, either version 3 of
 *    the License, or (at your option) any later version.
 *
 *    SimpleCppLogger is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with SimpleCppLogger.
 *    If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************
 * @endcode
 */
#ifndef JONMILLER131313_STRINGFORMATTER_HPP
#define JONMILLER131313_STRINGFORMATTER_HPP

#include <string>

#include "Formatter.hpp"
#include "jonmiller131313/logging_utils.hpp"

namespace jonmiller131313::logging {

    /// Class for formatting log entries into strings.
    class StringFormatter : public Formatter {
        public:
            /// The character used for annotating format tags.
            const static auto FORMAT_CHAR = '%';

            /**
             * @brief The constructor.
             *
             * @param strFormat The format template string.
             * @param dateTimeFormat The date and time format.
             *
             * @see setFormat()
             */
            explicit StringFormatter(std::string strFormat = "%T:%N:", std::string dateTimeFormat = "%F %X");

            /**
             * @brief C-String constructor, this allows implicit conversions from hardcoded strings.
             *
             * @param strFormat The format template string.
             *
             * @see setFormat()
             */
            explicit StringFormatter(const char *strFormat);

            /// The copy constructor.
            StringFormatter(const StringFormatter &cpy) = default;

            /// The move constructor.
            StringFormatter(StringFormatter &&mv) = default;

            /// The destructor.
            ~StringFormatter() override = default;

            /// The copy assignment operator.
            StringFormatter& operator=(const StringFormatter &cpy) = default;

            /// The move assignment operator.
            StringFormatter& operator=(StringFormatter &&cpy) = default;

            /**
             * @brief Sets the format template string.
             *
             * The structure of a format tag is `"%[num_chars]{tag}"` where `num_chars` is the number of characters the
             * tag should take up at minimum, if the data filling in the tag is less than that number then spaces are
             * inserted before it. If it is greater then it just formats normally. `tag` is a letter in the following
             * table:
             *
             *
             * @verbatim
             Log Tags:

             %% - The '%' character
             %D - Date and time from the date/time format
             %T - Log level tag
             %L - Log level tag, condensed (first character)
             %F - File that added the log entry
             %f - Function that added the log entry
             %l - Line number that added the log entry
             %N - Logger's name
             %n - Executing program's name (no path)
             %p - Executing program's PID
             %M - The log message, if not specified it appends it to the end of
                  the format string.

             Example:
             %10F:%4l -> "  test.cpp: 157"
             @endverbatim
             *
             * @param newFormat The new format template string.
             *
             * @see setDateTimeFormat()
             */
            void setFormat(std::string newFormat);

            /**
             * @brief Gets the format template string.
             *
             * @return The format string.
             */
            [[nodiscard]] auto getFormat() const -> const std::string&;

            [[nodiscard]] auto format(const LogEntry &entry) const -> std::string override;

        protected:
            /// The format template string.
            std::string strFormat;
    };
}


#endif

