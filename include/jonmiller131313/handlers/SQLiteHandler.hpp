/**
 * @file SQLiteHandler.hpp
 * @brief Handler to write log entries into an SQLite3 database.
 */
#ifndef JONMILLER131313_HANDLER_SQLITEHANDLER_HPP
#define JONMILLER131313_HANDLER_SQLITEHANDLER_HPP

#include <filesystem>
#include <utility>

#include <sqlite3.h>

#include "jonmiller131313/handlers/Handler.hpp"

namespace fs = std::filesystem;

namespace jonmiller131313::logging {
    /// Handler for writing to a table in a SQLite3 database.
    class SQLiteHandler : public Handler {
        public:
            /// The default database file if one isn't supplied in the constructor.
            static const fs::path DEFAULT_DB_FILE;
            /// The log table name.
            static const std::string LOG_TABLE_NAME;

            /**
             * @brief The constructor.
             *
             * @param ptr   Pointer to an already opened SQLite3 database, if left `nullptr` then a basic logging
             *              database will be created.
             * @param level The filtering log level to ignore.
             *
             * @throws std::runtime_error If any error was encountered while preparing the SQLite3 handles.
             */
            explicit SQLiteHandler(std::shared_ptr<sqlite3> ptr = nullptr, log_level level = NOT_SET);

            /// No copy constructor.
            SQLiteHandler(const SQLiteHandler &cpy) = delete;

            /// The move constructor
            SQLiteHandler(SQLiteHandler &&mv) = default;

            /// No copy assignment.
            SQLiteHandler& operator=(const SQLiteHandler &cpy) = delete;

            /// The move assignment operator.
            SQLiteHandler& operator=(SQLiteHandler &&mv) = default;

            void handleEntry(const LogEntry &entry) const override;

        private:
            /// The handle to the SQLite3 database.
            std::shared_ptr<sqlite3> sqlHandle;

            /// Shortcut for deleter function `sqlite3_finalize`.
            typedef int (*SqlStmtDeleter)(sqlite3_stmt*);
            /// Handle for the prepared SQLite3 statement.
            std::unique_ptr<sqlite3_stmt, SqlStmtDeleter> sqlStatement;
    };

}

#endif
