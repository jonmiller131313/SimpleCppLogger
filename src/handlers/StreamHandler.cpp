#include "jonmiller131313/handlers/StreamHandler.hpp"

namespace jonmiller131313::logging {

void StreamHandler::handleEntry(const LogEntry &entry) const {
    const auto formattedEntry = formatter->format(entry);

    stream->write(formattedEntry.c_str(), static_cast<std::streamsize>(formattedEntry.size()));
}

void StreamHandler::flush() const {
    stream->flush();
}

}
