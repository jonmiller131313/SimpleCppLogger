cmake_minimum_required(VERSION 3.16)

option(ENABLE_SQLITE "Enable logging to SQLite3 databases." YES)

include(FindSQLite3)

if(NOT ${SQLite3_FOUND})
    message(WARNING "Unable to find SQLite3 library, feature disabled. Suppress this warning with -DENABLE_SQLITE=NO")
    set(ENABLE_SQLITE NO)
endif()
