cmake_minimum_required(VERSION 3.16)

option(ENABLE_TESTS "Enable unit and regression test generation with 'check' command." NO)
if(NOT ENABLE_TESTS)
    return()
endif()

find_package(GTest REQUIRED)

if(${GTEST_FOUND})
    enable_testing()
    include(GoogleTest)

    add_subdirectory(${TEST_DIR})

    add_executable(${TEST_TARGET} EXCLUDE_FROM_ALL ${TEST_FILES})
    target_link_libraries(${TEST_TARGET} ${LIBRARY_TARGET} GTest::GTest GTest::Main)
    set_target_properties(${TEST_TARGET} PROPERTIES
        CXX_STANDARD 17
        CXX_STANDARD_REQUIRED YES
        INCLUDE_DIRECTORIES "${INCLUDE_DIR}"
    )
    if(${ENABLE_SQLITE})
        target_compile_definitions(${TEST_TARGET} PRIVATE ENABLE_SQLITE)
    endif()

    # use `ctest` in the build directory to perform tests.
    gtest_discover_tests(${TEST_TARGET} TEST_LIST TESTS)
endif()
