#include "jonmiller131313/handlers/SyslogHandler.hpp"

#include <algorithm>
#include <unordered_map>
#include <utility>

#include <syslog.h>

namespace jonmiller131313::logging {

const std::map<log_level, syslog_level> SyslogHandler::LEVEL_MAPPING = {
    {FATAL,    LOG_EMERG},
    {CRITICAL, LOG_CRIT},
    {ERROR,    LOG_ERR},
    {WARNING,  LOG_WARNING},
    {INFO,     LOG_NOTICE},
    {VERBOSE,  LOG_INFO},
    {DEBUG,    LOG_DEBUG},
    {NOT_SET,  LOG_DEBUG}
};

auto SyslogHandler::getSyslogLevel(log_level level) -> syslog_level {
    auto l = std::find_if_not(LEVEL_MAPPING.crbegin(), LEVEL_MAPPING.crend(),
                              [level] (const auto &mapPair) {
                                  return level < mapPair.first;
                              });

    return l != LEVEL_MAPPING.crend() ? l->second : LOG_EMERG;
}

void SyslogHandler::handleEntry(const LogEntry &entry) const {
    auto formattedEntry = formatter->format(entry);
    formattedEntry.pop_back(); // Remove trailing newline

    syslog(getSyslogLevel(entry.logLevel), "%s", formattedEntry.c_str());
}

}
