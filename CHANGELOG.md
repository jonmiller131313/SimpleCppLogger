# Simple C++ Logger Changelog
This document's purpose is to lay out and explain what exactly in the library changes over time, new features that are
added, and deprecations/removals.

### Versioning
An attempt at [Semantic Versioning](https://semver.org/) is used for the library version number, where the version
number is `XXX.YYY.ZZZ`. `XXX` corresponds to the major number, and increments of that value are not backwards 
compatible. `YYY` corresponds to the minor number, where features have been added in a backwards compatible manner
(code utilizing this library should not have to change **at all**). `ZZZ` is the patch number purely for bug fixes that
are completely backwards compatible.

#### Types of changes:
Changelog format type is inspired by [keep a changelog](https://keepachangelog.com).
 - `Added` for new features
 - `Changed` for changes in existing functionality
 - `Deprecated` for soon-to-be removed features
 - `Removed` for now removed features
 - `Fixed` for any bug fixes
 - `Security` for vulnerabilities
 - `Bug/Regression` for known bugs/regressions


## Incomplete/To Do
 - `formatters`:
   - `StringFormatter`:
     - Add option for spaces to fill width after tag.
     - Add width alignment.

## v5.1.0
 - `Changed`:
   - `Logger`: `Logger::basicConfig()` does nothing if handlers have already been added.
 - `Fixed`:
   - `Logger`: Re-implemented `Logger::globalConfig()`.

## v5.0.1
 - `Fixed`: CMake find module accidentally included an extra '.' in library file extension.

## v5.0.0
 - `Added`:
   - `Handler`: Added `SQLiteHandler` to write to database files.
   - `Logger`: Added static root logger object.
 - `Changed`:
   - Modernized code (`auto`, `std::move()`, etc...).
   - Library now builds as a shared library

## v4.0.0
 - `Changed`:
   - Changed license to LGPL v3+.

## v3.3.0
 - `Added`:
   - `Logger`: Global message and date/time format and log level.
 - `Fixed`:
   - A large portion of this library could not be used in static initialization/constructor attributed functions that
     run before `main()`. That has been fixed.

## v3.2.0
 - `Changed`:
   - `Handler`: Renamed `emit()` to `handleEntry()` for compatibility for Qt libraries.

## v3.1.1
 - `Changed`:
    - Documentation

## v3.1
 - `Added`:
   - `Logger`: Added a `basicConfig()` to quickly output to stdout.
   - `Logger`: If a logger object has no handler and `addEntry()` is called then a default `StdOutHandler()` is added.

## v3.0
 - `Changed`:
   - Moved `about.hpp` to `logging.hpp` and add more include files so that this is the only file needed to interface
     with the library.

## v2.0
 - `Added`:
   - `Handler`: Added handler classes so now loggers can have output to multiple outputs.
     - Output to `stdout`/`stderr`
     - Output to files
     - Output to system log
   - `Formatter`: Added formatter classes for easier extendability and customization.
   - `Tests`: Added unit tests
   - `CMake`: Add build scripts for `install` target.
 - `Changed`:
   - Changed namespace to `jonmiller131313::logger`.
   - `Logger`: Due to the new handler and formatter classes a major refactor of code was done and restructuring of the 
               library's API interface.
   - `Logger`: No more dynamic allocation for each new `Logger` created.
   - JSON output is no longer in a list, now each entry is an independent object.
 - `Regression`:
   - `Documentation`: No examples at the moment.

## v1.0.2
 - `Fixed`:
   - `CMake`: Fix variable name.

## v1.0.1
 - `Fixed`:
   - `about`: Fixed compile issues with constant variables.

## v1.0.0
 - Initial import
