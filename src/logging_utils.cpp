#include "jonmiller131313/logging_utils.hpp"

#include <algorithm>
#include <filesystem>
#include <unordered_map>

namespace jonmiller131313::logging {

namespace fs = std::filesystem;

/// All of the registered log levels and their names.
static auto logLevelTags = std::unordered_map<log_level, std::string>({
    {FATAL, "FATAL"},
    {CRITICAL, "CRITICAL"},
    {ERROR, "ERROR"},
    {WARNING, "WARNING"},
    {INFO, "INFO"},
    {VERBOSE, "VERBOSE"},
    {DEBUG, "DEBUG"},
    {NOT_SET, "NOT_SET"}
});

auto getLogLevelName(log_level level) -> const std::string& {
    return logLevelTags.at(level);
}

auto getLogLevelValue(const std::string &levelName) -> log_level {
    auto it = std::find_if(logLevelTags.cbegin(), logLevelTags.cend(), [&levelName] (const auto &it) {
        return it.second == levelName;
    });
    if(it != logLevelTags.cend())
        return it->first;
    else
        throw std::out_of_range("Log level '" + levelName + "' not registered.");
}

void registerLogLevel(log_level level, const std::string& levelName) {
    logLevelTags.insert_or_assign(level, levelName);
}

auto getExecutingFileName() -> const std::string& {
    static const auto execFile = [] () -> std::string {
        try {
            return fs::read_symlink("/proc/self/exe").filename().string();
        } catch(const fs::filesystem_error &) {
            return "!!!getExecutingFileName() ERROR!!!";
        }
    }();

    return execFile;
}

}
