/**
 * @file logging.hpp
 * @brief Top level include file, user shouldn't need any other file unless
 *        extending functionality.
 * @copyright LGPLv3+ (c) 2019 Jonathan Miller
 * @code{.md}
 ***********************************************************************
 * This file is part of SimpleCppLogger:
 *    https://gitlab.com/jonmiller131313/SimpleCppLogger
 *
 *    SimpleCppLogger is free software: you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public License
 *    as published by the Free Software Foundation, either version 3 of
 *    the License, or (at your option) any later version.
 *
 *    SimpleCppLogger is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with wiringPi.
 *    If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************
 * @endcode
 */
#ifndef JONMILLER131313_LOGGING_HPP
#define JONMILLER131313_LOGGING_HPP

#include "jonmiller131313/formatters/JsonFormatter.hpp"
#include "jonmiller131313/formatters/StringFormatter.hpp"
#include "jonmiller131313/Logger.hpp"
#include "jonmiller131313/logging_utils.hpp"
#include "jonmiller131313/handlers/FileHandler.hpp"
#include "jonmiller131313/handlers/SQLiteHandler.hpp"
#include "jonmiller131313/handlers/SyslogHandler.hpp"

namespace jonmiller131313::logging {
    /// The full version string.
    const char *const VERSION_STR = "5.1.0";
    /// The major version number.
    const int VERSION_MAJOR = 5;
    /// The minor version number.
    const int VERSION_MINOR = 1;
    /// The patch version number.
    const int VERSION_PATCH = 0;
}

#endif
