#include <gtest/gtest.h>

#include <filesystem>
#include <iostream>

#include <ctime>


#include "jonmiller131313/logging.hpp"

using namespace jonmiller131313::logging;
namespace gtest = ::testing::internal;
namespace fs = std::filesystem;

const static auto timestamp = std::time_t(15000);
const static auto TEST_FILE = fs::path("test.txt");

class StreamHandlerTest : public ::testing::Test {
    protected:
        const LogEntry debugLogEntry;
        const LogEntry errorLogEntry;

        StreamHandlerTest() :
                debugLogEntry("This is a debug message.",
                              DEBUG,
                              "formatter.cpp",
                              "unit_test",
                              25,
                              "logger_name",
                              timestamp,
                              "a.out"),
                errorLogEntry("This is an error message.",
                              ERROR,
                              "formatter.cpp",
                              "unit_test",
                              25,
                              "logger_name",
                              timestamp,
                              "a.out") {}
};


class FileHandlerTest : public StreamHandlerTest {};

class SQLiteHandlerText : public StreamHandlerTest {};


TEST_F(StreamHandlerTest, StdOut) {
    auto coutHandler = StdOutHandler();
    gtest::CaptureStdout();
    coutHandler->handle(debugLogEntry);

    EXPECT_EQ(gtest::GetCapturedStdout(), "DEBUG:logger_name:This is a debug message.\n");
}

TEST_F(StreamHandlerTest, StdErr) {
    auto cerrHandler = StdErrHandler();
    gtest::CaptureStderr();
    cerrHandler->handle(debugLogEntry);

    EXPECT_EQ(gtest::GetCapturedStderr(), "DEBUG:logger_name:This is a debug message.\n");
}

TEST_F(StreamHandlerTest, SetLogLevel) {
    auto coutHandler = StdOutHandler("%M", ERROR);
    gtest::CaptureStdout();
    coutHandler->handle(debugLogEntry);
    coutHandler->handle(errorLogEntry);

    EXPECT_EQ(gtest::GetCapturedStdout(), "This is an error message.\n");
}

TEST_F(FileHandlerTest, FileOutput) {
    auto fHandler = FileHandler(TEST_FILE, StringFormatter("%M"), true);
    auto lineBuff = std::array<char, 1024>();
    fHandler.handle(debugLogEntry);

    auto inFile = std::ifstream(TEST_FILE);
    inFile.read(lineBuff.data(), lineBuff.size());

    EXPECT_EQ(inFile.gcount(), debugLogEntry.message.size() + 1); // message + \n
    EXPECT_TRUE(inFile.eof());
    EXPECT_STREQ(lineBuff.data(), "This is a debug message.\n");
    if(!::testing::Test::HasFailure()) {
        EXPECT_NO_THROW(EXPECT_TRUE(fs::remove(TEST_FILE)));
    }
}

#ifdef ENABLE_SQLITE
TEST_F(SQLiteHandlerText, BasicTest) {
    auto dbHandler = SQLiteHandler();

    dbHandler.handle(debugLogEntry);
    dbHandler.handle(errorLogEntry);

    // DOES NOT ACTUALLY CHECK ANYTHING.
    // INSTEAD, CREATES A DATABASE FILE IN THE CURRENT DIRECTORY.
}
#endif