#include "jonmiller131313/formatters/JsonFormatter.hpp"

#include <regex>
#include <string>

#include <unistd.h>

namespace jonmiller131313::logging {

JsonFormatter::JsonFormatter() :
    Formatter("%FT%X") {}

auto JsonFormatter::format(const LogEntry &entry) const -> std::string {
    return std::string("{") +
        R"("timestamp":")" + getFormattedDateTime(entry.logTime) + "\","+
        R"("log_level":")" + getLogLevelName(entry.logLevel) + "\"," +
        R"("file":")" + entry.fileName + "\"," +
        R"("function":")" + entry.funcName + "()\"," +
        R"("line":)" + std::to_string(entry.lineNum) + "," +
        R"("logger_name":")" + entry.loggerName + "\"," +
        R"("executing_file":")" + entry.execFile + "\"," +
        R"("pid":)" + std::to_string(getpid()) + "," +
        R"("message":")" + std::regex_replace(entry.message, std::regex("\""), "\\\"") + '"' +
        "}\n";
}

}
