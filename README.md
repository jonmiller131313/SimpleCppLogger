# Simple C++ Logger Library
[![Version](https://img.shields.io/badge/Version-5.1.0-informational.svg)](CHANGELOG.md)

The goal of this library is to implement a logging library inspired by [Python's logging module](https://docs.python.org/3/library/logging.html).

## Getting Started
### Dependencies:
 - C++17
   - Including `std::filesystem` support
 - CMake 3.10
 - [Doxygen](https://doxygen.nl/) _(optional)_
 - [GoogleTest](https://github.com/google/googletest) _(optional)_

 ### Building/Installing
Clone the repo, then run:
```bash
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make -j
sudo make install
```

#### CMake options:
 - `-DENABLE_TESTS=YES`: Enables unit test with `check` make target
   - Requires GoogleTest
 - `-DDISABLE_DOCS=YES`: Disable generating documentation with `docs` target
   - Documentation generation is enabled by default, use this option to suppress missing Doxygen dependency warning.


## Including In Your Project
### CMake
If using the default CMake install location all you need to include in your CMakeLists.txt file is
```cmake
find_package(SimpleCppLogger)
...
target_link_libraries(${TARGET} SimpleCppLogger)
```
If it does not find the package, or you installed the library in a non-default location make sure to set the `-DCMAKE_PREFIX_PATH` variable or the `PATHS` option in `find_package()`.

### pkgconf
This project also includes a `SimpleCppLogger.pc` file. By default on UNIX system it installs it to `/usr/local/lib/pkgconfig` so that path will need to be set in the `PKG_CONFIG_PATH` environmental variable before the `pkgconf` command is run so that the command can find the file.

**Usage:**
```bash
g++ source.cpp -c $(pkgconf --cflags SimpleCppLogger) -o source.cpp.o
...
g++ source.cpp.o $(pkgconf --libs SimpleCppLogger) -o a.out
```
