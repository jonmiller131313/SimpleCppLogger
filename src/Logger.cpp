#include "jonmiller131313/Logger.hpp"

#include <utility>

#include "jonmiller131313/handlers/StreamHandler.hpp"

namespace jonmiller131313::logging {

std::unordered_map<std::string, Logger> Logger::logList;
std::mutex Logger::logListLock;
std::optional<GlobalConfig> Logger::config;

Logger Logger::rootLogger("root");

Logger::Logger(std::string name, log_level level) :
        name(std::move(name)),
        logLevel(level) {
}

Logger::Logger(Logger &&mvLogger) noexcept : logLevel(NOT_SET) {
    *this = std::move(mvLogger);
}

Logger& Logger::operator=(Logger &&mvLogger) noexcept {
    auto lock = std::lock_guard(mvLogger.logLock);

    name = std::move(mvLogger.name);
    logLevel = mvLogger.logLevel;
    handlers = std::move(mvLogger.handlers);

    return *this;
}

auto Logger::getRootLogger() -> Logger& {
    return rootLogger;
}

void Logger::globalConfig(std::string format, log_level level) {
    config = GlobalConfig();
    config->format = format;
    config->level = level;

    getRootLogger().basicConfig(std::move(format), level);
}

auto Logger::getLogger(const std::string &name) -> Logger& {
    return getLogger(name, NOT_SET);
}

auto Logger::getLogger(const std::string &name, log_level level) -> Logger& {
    auto lock = std::lock_guard(logListLock);

    const auto [it, isNew] = logList.emplace(name, Logger(name, level));
    auto &logger = it->second;

    if(isNew && config)
        logger.basicConfig(config->format, config->level);

    return logger;
}

void Logger::basicConfig(std::string fmt, log_level level) {
    auto lock = std::lock_guard(logLock);
    if(handlers.empty())
        handlers.push_back(StdOutHandler(StringFormatter(std::move(fmt)), level));
}

void Logger::clearLoggers() {
    auto lock = std::lock_guard(logListLock);
    logList.clear();
}

void Logger::addHandler(std::unique_ptr<Handler> handlerPtr) {
    auto lock = std::lock_guard(logLock);

    handlers.push_back(std::move(handlerPtr));
}

void Logger::addHandlers(std::vector<std::unique_ptr<Handler>> &&newHandlers) {
    auto lock = std::lock_guard(logLock);
    std::move(newHandlers.begin(), newHandlers.end(), std::back_inserter(handlers));
}

void Logger::setLevel(log_level level) {
    std::lock_guard<std::mutex> lock(logLock);
    logLevel = level;
}

void Logger::addEntry(log_level level,
                      std::string &&message,
                      std::string &&fileName,
                      std::string &&funcName,
                      unsigned int lineNum) const {
    static const auto strFmtr = StringFormatter();
    auto lock = std::lock_guard(logLock);

    if(level < logLevel)
        return;

    auto entry = LogEntry(std::move(message), level, std::move(fileName), std::move(funcName), lineNum, name);

    if(handlers.empty()) {
        auto fmtM = strFmtr.format(entry);
        std::cout.write(fmtM.c_str(), static_cast<std::streamsize>(fmtM.size()));
        return;
    }

    for(const auto &handler : handlers) {
        handler->handle(entry);
    }
}

log_level Logger::getLevel() const {
    auto lock = std::lock_guard(logLock);
    return logLevel;
}

auto Logger::getName() const -> const std::string& {
    auto lock = std::lock_guard(logLock);
    return name;
}


}
