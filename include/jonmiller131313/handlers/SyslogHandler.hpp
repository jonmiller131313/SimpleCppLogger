/**
 * @file SyslogHandler.hpp
 * @brief Handler for logging to the system log.
 * @copyright LGPLv3+ (c) 2019 Jonathan Miller
 * @code{.md}
 ***********************************************************************
 * This file is part of SimpleCppLogger:
 *    https://gitlab.com/jonmiller131313/SimpleCppLogger
 *
 *    SimpleCppLogger is free software: you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public License
 *    as published by the Free Software Foundation, either version 3 of
 *    the License, or (at your option) any later version.
 *
 *    SimpleCppLogger is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with SimpleCppLogger.
 *    If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************
 * @endcode
 */
#ifndef JONMILLER131313_SYSLOGHANDLER_HPP
#define JONMILLER131313_SYSLOGHANDLER_HPP

#include <map>

#include "jonmiller131313/formatters/StringFormatter.hpp"
#include "jonmiller131313/handlers/Handler.hpp"
#include "jonmiller131313/logging_utils.hpp"

namespace jonmiller131313::logging {

    using syslog_level = int;

    /// Class for writing log entries to syslog.
    class SyslogHandler : public Handler {
        public:

            /**
             * @brief The constructor
             *
             * @param formatter The formatter to use.
             * @param level The level to filter entries.
             */
            template<class F = StringFormatter,
                     class C = std::enable_if_t<std::is_base_of_v<Formatter, F>>>
            explicit SyslogHandler(F &&formatter = F(), log_level level = NOT_SET) :
                    Handler(std::forward<F>(formatter), level) {
                SyslogHandler::open();
            }

            /// No duplicate handlers.
            SyslogHandler(const SyslogHandler& cpy) = delete;

            /// The move constructor.
            SyslogHandler(SyslogHandler &&mv) = default;

            /// The destructor.
            ~SyslogHandler() override = default;

            /// No copy assignment operator.
            SyslogHandler& operator=(const SyslogHandler &cpy) = delete;

            /// The move assignment operator.
            SyslogHandler& operator=(SyslogHandler &&mv) = default;

            void handleEntry(const LogEntry &entry) const override;

        protected:
            /**
             * @brief A mapping of @ref log_level to syslog's log level
             *        integers.
             *
             *    `log_level` | `syslog()` log levels
             * ---------------|----------------------
             * @ref     FATAL | `LOG_EMERG`
             * @ref  CRITICAL | `LOG_CRIT`
             * @ref     ERROR | `LOG_ERR`
             * @ref   WARNING | `LOG_WARNING`
             * @ref      INFO | `LOG_NOTICE`
             * @ref   VERBOSE | `LOG_INFO`
             * @ref    DEBUG  | `LOG_DEBUG`
             * @ref   NOT_SET | `LOG_DEBUG`
             *
             * @see getSyslogLevel()
             * @see `man 3 syslog`
             */
            static const std::map<log_level, syslog_level> LEVEL_MAPPING;

            /**
             * @brief Gets the closest syslog level to `level`.
             *
             * @param level The reference @ref log_level.
             *
             * @return A valid integer to pass into `syslog()`
             *
             * @see LEVEL_MAPPING
             * @see `man 3 syslog`
             */
            static auto getSyslogLevel(log_level level) -> syslog_level ;
    };

}

#endif
