#include "jonmiller131313/handlers/Handler.hpp"

namespace jonmiller131313::logging {

Handler::Handler(const char *fmt, log_level level) :
        formatter(std::make_unique<StringFormatter>(fmt)), logLevel(level) {
}

void Handler::setLevel(log_level newLevel) noexcept {
    logLevel = newLevel;
}

log_level Handler::getLevel() const noexcept {
    return logLevel;
}

auto Handler::getFormatter() const -> const std::unique_ptr<Formatter>& {
    return formatter;
}

void Handler::handle(const LogEntry &entry) const {
    if(entry.logLevel >= logLevel)
        this->handleEntry(entry);
}

void Handler::open() {
}

void Handler::flush() const {
}

void Handler::close() {
}

}
