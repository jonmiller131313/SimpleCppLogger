/**
 * @file FileHandler.hpp
 * @brief Handler for writing to files.
 * @copyright LGPLv3+ (c) 2019 Jonathan Miller
 * @code{.md}
 ***********************************************************************
 * This file is part of SimpleCppLogger:
 *    https://gitlab.com/jonmiller131313/SimpleCppLogger
 *
 *    SimpleCppLogger is free software: you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public License
 *    as published by the Free Software Foundation, either version 3 of
 *    the License, or (at your option) any later version.
 *
 *    SimpleCppLogger is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with SimpleCppLogger.
 *    If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************
 * @endcode
 */
#ifndef JONMILLER131313_FILEHANDLER_HPP
#define JONMILLER131313_FILEHANDLER_HPP

#include <filesystem>
#include <fstream>
#include <utility>

#include "jonmiller131313/handlers/StreamHandler.hpp"
#include "jonmiller131313/logging_utils.hpp"

namespace jonmiller131313::logging {

    /// Namespace alias for C++'s filesystem library.
    namespace fs = std::filesystem;

    /// Handler for writing to files.
    class FileHandler : public StreamHandler<std::ofstream> {
        public:
            /**
             * @brief The constructor.
             *
             * Automatically opens the file stream.
             *
             * @param filePath The path of the output file to write to.
             * @param overwrite If we should truncate the file before writing to it.
             * @param formatter The formatter.
             * @param level The level filter.
             */
            template<class F = StringFormatter>
            explicit FileHandler(fs::path filePath,
                                 F &&formatter = F(),
                                 bool overwrite = false,
                                 log_level level = NOT_SET) :
                    StreamHandler(std::ofstream(), std::forward<F>(formatter), level),
                    filePath(std::move(filePath)),
                    overwrite(overwrite) {
                FileHandler::open();
            }

            /// No duplicate handlers.
            FileHandler(const FileHandler &cpy) = delete;

            /// The move constructor.
            FileHandler(FileHandler &&mv) noexcept;

            /**
             * @brief The destructor.
             *
             * Flushes and closes the file.
             */
            ~FileHandler() override = default;

            /// No copy assignment operator.
            FileHandler& operator=(const FileHandler &cpy) = delete;

            /// The move assignment operator.
            FileHandler& operator=(FileHandler &&mv) noexcept;

            /**
             * @brief Gets the output file's path.
             *
             * @return The file's path.
             */
            [[nodiscard]] auto getPath() const -> const fs::path&;

            /**
             * @brief Opens the file.
             *
             * If @ref overwrite is set then the file is truncated.
             */
            void open() override;

            /// Flushes and closes the file.
            void close() override;

        protected:
            /// The output file's path.
            fs::path filePath;

            /// Flag that truncates the file when set.
            bool overwrite;

    };
}

#endif
