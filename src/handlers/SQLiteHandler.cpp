#include "jonmiller131313/handlers/SQLiteHandler.hpp"

#include <cstring>

using namespace jonmiller131313::logging;
using namespace std::string_literals;

const fs::path SQLiteHandler::DEFAULT_DB_FILE = "log.db";
const std::string SQLiteHandler::LOG_TABLE_NAME = "logs";

static const auto SQL_CREATE_TABLE = R"SQL(CREATE TABLE logs (
    ID          INTEGER     PRIMARY KEY                 ,
    MESSAGE     TEXT                            NOT NULL,
    TIMESTAMP   CHAR(19)                        NOT NULL,
    LEVEL       INTEGER     CHECK(LEVEL >= 0)   NOT NULL,
    FILE_NAME   TEXT                            NOT NULL,
    FUNC_NAME   TEXT                            NOT NULL,
    LINE_NUM    INTEGER     CHECK(LINE_NUM > 0) NOT NULL,
    NAME        TEXT                            NOT NULL,
    PROG_NAME   TEXT                            NOT NULL
);
)SQL";

static const auto SQL_WRITE_LOG = "INSERT INTO " + SQLiteHandler::LOG_TABLE_NAME + R"SQL(
(MESSAGE, TIMESTAMP, LEVEL, FILE_NAME, FUNC_NAME, LINE_NUM, NAME, PROG_NAME)
VALUES (?, ?, ?, ?, ?, ?, ?, ?);
)SQL";

SQLiteHandler::SQLiteHandler(std::shared_ptr<sqlite3> ptr, log_level level) :
        Handler(StringFormatter(), level),
        sqlHandle(std::move(ptr)),
        sqlStatement(nullptr, sqlite3_finalize) {
    if(!sqlHandle) {
        auto p = static_cast<sqlite3*>(nullptr);
        if(const auto STATUS = sqlite3_open(DEFAULT_DB_FILE.c_str(), &p); STATUS != SQLITE_OK) {
            const auto ERR_MSG = "Unable to open database '" + DEFAULT_DB_FILE.string() + "': " +
                                 sqlite3_errmsg(sqlHandle.get());
            sqlite3_close(p);
            throw std::runtime_error(ERR_MSG);
        }
        sqlHandle = std::shared_ptr<sqlite3>(p, sqlite3_close);
    }

    const auto CHECK_TABLE_SQL = "SELECT 1 FROM sqlite_master WHERE type = 'table' AND name = '" + LOG_TABLE_NAME + "';";
    auto tableExists = false;
    auto errMsg = static_cast<char *>(nullptr);
    const auto CHECK_TABLE_CALLBACK = [] (void *arg, int numCols, char **colResults, char **) {
        auto &tableExists = *static_cast<bool *>(arg);
        if(numCols == 1 && std::strcmp(colResults[0], "1") == 0)
            tableExists = true;
        return 0;
    };

    if(auto status = sqlite3_exec(sqlHandle.get(),
                                  CHECK_TABLE_SQL.c_str(),
                                  CHECK_TABLE_CALLBACK,
                                  static_cast<void *>(&tableExists),
                                  &errMsg);
       status != SQLITE_OK) {
        const auto ERR_MSG = "Unable to check if table '" + LOG_TABLE_NAME + "' exists: " +
                             sqlite3_errmsg(sqlHandle.get());
        sqlite3_free(errMsg);
        throw std::runtime_error(ERR_MSG);
    } else if(!tableExists) {
        if(status = sqlite3_exec(sqlHandle.get(),
                                 SQL_CREATE_TABLE,
                                 nullptr,
                                 nullptr,
                                 &errMsg);
           status != SQLITE_OK) {
            const auto ERR_MSG = "Unable to create table '" + LOG_TABLE_NAME + "': " +
                                 sqlite3_errmsg(sqlHandle.get());
            sqlite3_free(errMsg);
            throw std::runtime_error(ERR_MSG);
        }
    }

    auto p = static_cast<sqlite3_stmt *>(nullptr);
    if(const auto STATUS = sqlite3_prepare_v3(sqlHandle.get(),
                                              SQL_WRITE_LOG.c_str(),
                                              static_cast<int>(SQL_WRITE_LOG.size() + 1), // Include the null byte, per the documentation.
                                              SQLITE_PREPARE_PERSISTENT,
                                              &p,
                                              nullptr);
       STATUS != SQLITE_OK) {
        const auto ERR_MSG = "Unable to prepare the log SQL statement: "s + sqlite3_errmsg(sqlHandle.get());
        throw std::runtime_error(ERR_MSG);
    }
    sqlStatement = std::unique_ptr<sqlite3_stmt, SqlStmtDeleter>(p, sqlite3_finalize);
}

void SQLiteHandler::handleEntry(const LogEntry &entry) const {
    sqlite3_reset(sqlStatement.get());

    const auto FORMATTED_TIMESTAMP = formatter->getFormattedDateTime(entry.logTime);

    if(auto status = sqlite3_bind_text(sqlStatement.get(), 1, entry.message.c_str(), static_cast<int>(entry.message.size()), SQLITE_STATIC);
       status != SQLITE_OK) {
        throw std::runtime_error("Unable to bind MESSAGE parameter (" + std::to_string(status)+ "): " +
                                 sqlite3_errmsg(sqlHandle.get()));
    } else if(status = sqlite3_bind_text(sqlStatement.get(), 2, FORMATTED_TIMESTAMP.c_str(), static_cast<int>(FORMATTED_TIMESTAMP.size()), SQLITE_STATIC);
              status != SQLITE_OK) {
        throw std::runtime_error("Unable to bind TIMESTAMP parameter (" + std::to_string(status)+ "): " +
                                 sqlite3_errmsg(sqlHandle.get()));
    } else if(status = sqlite3_bind_int64(sqlStatement.get(), 3, entry.logLevel);
              status != SQLITE_OK) {
        throw std::runtime_error("Unable to bind LEVEL parameter (" + std::to_string(status)+ "): " +
                                 sqlite3_errmsg(sqlHandle.get()));
    } else if(status = sqlite3_bind_text(sqlStatement.get(), 4, entry.fileName.c_str(), static_cast<int>(entry.fileName.size()), SQLITE_STATIC);
              status != SQLITE_OK) {
        throw std::runtime_error("Unable to bind FILE_NAME parameter (" + std::to_string(status)+ "): " +
                                 sqlite3_errmsg(sqlHandle.get()));
    } else if(status = sqlite3_bind_text(sqlStatement.get(), 5, entry.funcName.c_str(), static_cast<int>(entry.funcName.size()), SQLITE_STATIC);
              status != SQLITE_OK) {
        throw std::runtime_error("Unable to bind FUNC_NAME parameter (" + std::to_string(status)+ "): " +
                                 sqlite3_errmsg(sqlHandle.get()));
    } else if(status = sqlite3_bind_int64(sqlStatement.get(), 6, entry.lineNum);
              status != SQLITE_OK) {
        throw std::runtime_error("Unable to bind LINE_NUM parameter (" + std::to_string(status)+ "): " +
                                 sqlite3_errmsg(sqlHandle.get()));
    } else if(status = sqlite3_bind_text(sqlStatement.get(), 7, entry.loggerName.c_str(), static_cast<int>(entry.loggerName.size()), SQLITE_STATIC);
              status != SQLITE_OK) {
        throw std::runtime_error("Unable to bind NAME parameter (" + std::to_string(status)+ "): " +
                                 sqlite3_errmsg(sqlHandle.get()));
    } else if(status = sqlite3_bind_text(sqlStatement.get(), 8, entry.execFile.c_str(), static_cast<int>(entry.execFile.size()), SQLITE_STATIC);
              status != SQLITE_OK) {
        throw std::runtime_error("Unable to bind PROG_NAME parameter (" + std::to_string(status) + "): " +
                                 sqlite3_errmsg(sqlHandle.get()));
    }

    auto status = 0;
    while((status = sqlite3_step(sqlStatement.get())) != SQLITE_DONE) {
        if(status != SQLITE_BUSY)
            throw std::runtime_error("Unable to commit to table (" + std::to_string(status) + "): " +
                                     sqlite3_errmsg(sqlHandle.get()));
    }
}
