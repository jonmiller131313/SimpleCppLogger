#include "jonmiller131313/formatters/StringFormatter.hpp"

#include <utility>

#include <climits>
#include <cstdlib>
#include <cctype>

#include <unistd.h>

namespace jonmiller131313::logging {

static inline auto formatSpaces(unsigned long numSpaces, const std::string &data) {
    if(numSpaces == 0)
        return data;

    auto formattedData = std::string();
    if(data.size() < numSpaces)
        formattedData.append(numSpaces - data.size(), ' ');

    return formattedData + data;
}

StringFormatter::StringFormatter(std::string strFormat, std::string dateTimeFormat) :
        Formatter(std::move(dateTimeFormat)),
        strFormat(std::move(strFormat)) {
    const static auto MESSAGE_TAG = std::string() + FORMAT_CHAR + 'M';

    if(this->strFormat.find(MESSAGE_TAG) == std::string::npos)
        this->strFormat.append(MESSAGE_TAG);
}

StringFormatter::StringFormatter(const char *strFormat) :
        StringFormatter(std::string(strFormat), "%F %X") {}

auto StringFormatter::getFormat() const -> const std::string& {
    return strFormat;
}

void StringFormatter::setFormat(std::string newFormat) {
    strFormat = std::move(newFormat);
}

auto StringFormatter::format(const LogEntry &entry) const -> std::string {
    auto formattedString = std::string();
    auto digitBuffer = std::string();
    auto numSpaces = 0UL;
    auto foundTag = false;

    for(auto ch : strFormat) {
        if(foundTag) {
            if(isdigit(ch)) {
                digitBuffer.push_back(ch);
                continue;
            } else if(!digitBuffer.empty()) {
                auto end = static_cast<char *>(nullptr);
                numSpaces = strtoul(digitBuffer.c_str(), &end, 10);

                if(end == digitBuffer.c_str() || numSpaces == ULONG_MAX)
                    numSpaces = 0;
                digitBuffer.clear();
            }
            switch(ch) {
                case 'L': { // Log level char
                    auto levelTag = getLogLevelName(entry.logLevel).substr(0, 1);
                    formattedString.append(formatSpaces(numSpaces, levelTag));
                    break;
                }
                case 'D': // Timestamp
                    formattedString.append(formatSpaces(numSpaces, getFormattedDateTime(entry.logTime)));
                    break;

                case 'T': // Log level
                    formattedString.append(formatSpaces(numSpaces, getLogLevelName(entry.logLevel)));
                    break;

                case 'F': { // File
                    auto pos = entry.fileName.rfind('/');
                    auto fileName = std::string();

                    if(pos == std::string::npos)
                        fileName = entry.fileName;
                    else
                        fileName = entry.fileName.substr(pos + 1);

                    formattedString.append(formatSpaces(numSpaces, fileName));
                    break;
                }
                case 'f': // Function
                    formattedString.append(formatSpaces(numSpaces, entry.funcName + "()"));
                    break;

                case 'l': // Line number
                    formattedString.append(formatSpaces(numSpaces, std::to_string(entry.lineNum)));
                    break;

                case 'N': // Logger name
                    formattedString.append(formatSpaces(numSpaces, entry.loggerName));
                    break;

                case 'n': // Executing file
                    formattedString.append(formatSpaces(numSpaces, entry.execFile));
                    break;

                case 'p': // PID
                    formattedString.append(formatSpaces(numSpaces, std::to_string(getpid())));
                    break;

                case 'M': // Message
                    formattedString.append(formatSpaces(numSpaces, entry.message));
                    break;

                case FORMAT_CHAR:
                    formattedString.push_back(FORMAT_CHAR);
                    break;

                default:
                    // Do nothing.
                    ;
            }
            foundTag = false;
            numSpaces = 0;
        } else {
            if(ch == FORMAT_CHAR)
                foundTag = true;
            else
                formattedString.push_back(ch);
        }
    }

    return formattedString + '\n';
}

}
