#include "jonmiller131313/formatters/Formatter.hpp"

#include <iomanip>
#include <string>
#include <utility>

#include <ctime>

namespace jonmiller131313::logging {

Formatter::Formatter(std::string dateTimeFormat) :
    dateTimeFormat(std::move(dateTimeFormat)) {
}

auto Formatter::getDateTimeFormat() const -> const std::string& {
    return dateTimeFormat;
}

auto Formatter::getFormattedDateTime(std::time_t timestamp) const -> std::string {
    auto formattedDateTime = std::stringstream();
    auto tm = localtime(&timestamp);

    formattedDateTime << std::put_time(tm, dateTimeFormat.c_str());
    return formattedDateTime.str();
}

void Formatter::setDateTimeFormat(const std::string &newDateTimeFormat) {
    dateTimeFormat = newDateTimeFormat;
}

}