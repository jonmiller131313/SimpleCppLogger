#include <gtest/gtest.h>

#include <string>

#include "jonmiller131313/logging_utils.hpp"

using namespace jonmiller131313::logging;

using namespace std::string_literals;

static const auto TEST_BIN_NAME = "SimpleCppLogger_test";

TEST(LoggingUtils, RegisteringLogLevels) {
    const auto NEW_LEVEL_NUM = static_cast<log_level>(DEBUG + 1);
    const auto NEW_LEVEL_NAME = "TEST"s;

    registerLogLevel(NEW_LEVEL_NUM, NEW_LEVEL_NAME);

    ASSERT_NO_THROW({
        EXPECT_EQ(getLogLevelName(NEW_LEVEL_NUM), NEW_LEVEL_NAME);
        EXPECT_EQ(getLogLevelValue(NEW_LEVEL_NAME), NEW_LEVEL_NUM);
    });


}

TEST(LoggingUtils, InvalidLogLevels) {
    EXPECT_THROW(static_cast<void>(getLogLevelName(NOT_SET + 1)), std::out_of_range);
    EXPECT_THROW(static_cast<void>(getLogLevelValue("nonexistent")), std::out_of_range);
}

TEST(LoggingUtils, ExecutingFileName) {
    EXPECT_STREQ(getExecutingFileName().c_str(), TEST_BIN_NAME);
}

