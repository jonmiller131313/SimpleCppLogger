/**
 * @file logging_utils.hpp
 * @brief Log entry class and utility values/functions.
 * @copyright LGPLv3+ (c) 2019 Jonathan Miller
 * @code{.md}
 ***********************************************************************
 * This file is part of SimpleCppLogger:
 *    https://gitlab.com/jonmiller131313/SimpleCppLogger
 *
 *    SimpleCppLogger is free software: you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public License
 *    as published by the Free Software Foundation, either version 3 of
 *    the License, or (at your option) any later version.
 *
 *    SimpleCppLogger is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with SimpleCppLogger.
 *    If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************
 * @endcode
 */
#ifndef JONMILLER131313_LOGENTRY_HPP
#define JONMILLER131313_LOGENTRY_HPP

#include <string>

#include <ctime>
#include <utility>

namespace jonmiller131313::logging {

    /// Shortcut to designate a log level type.
    using log_level = unsigned int;

    /// Fatal errors before exiting.
    const log_level FATAL = 100;
    /// Critical errors that _may_ be recoverable with human intervention.
    const log_level CRITICAL = 50;
    /// Basic non-fatal error reporting.
    const log_level ERROR = 40;
    /// Warnings
    const log_level WARNING = 30;
    /// Basic information.
    const log_level INFO = 20;
    /// Verbose information.
    const log_level VERBOSE = 15;
    /// Developer defined level 1.
    const log_level DEBUG = 10;
    /// If a level is not set, globally captures all levels.
    const log_level NOT_SET = 0;

    /**
     * @brief Gets the log level's string representation.
     *
     * @param level The log level.
     *
     * @return The level's name.
     *
     * @throws std::out_of_range If `level` is not registered.
     *
     * @see registerLogLevel()
     */
    [[nodiscard]] auto getLogLevelName(log_level level) -> const std::string&;

    /**
     * @brief Gets the log level's numerical representation.
     *
     * @param levelName The log level's name.
     *
     * @return The level's numerical value.
     *
     * @throws std::out_of_range If `levelName` is not registered.
     *
     * @see registerLogLevel()
     */
    [[nodiscard]] auto getLogLevelValue(const std::string &levelName) -> log_level;

    /**
     * @brief Registers a new log level, or overwrites a previous level.
     *
     * @param level The numerical representation of the level.
     * @param levelName The name of the level.
     */
    void registerLogLevel(log_level level, const std::string &levelName);

    /**
    * @brief Gets the executing file name.
    *
    * @return The executing file name, or an error string on error.
    */
    auto getExecutingFileName() -> const std::string&;

    /// Class holding log entry data, is basically a glorified struct.
    class LogEntry {
        public:
            /// The log message.
            const std::string message;
            /// The creation of the log entry.
            const std::time_t logTime;
            /// The entry's level.
            const log_level logLevel;
            /// The file that generated the entry.
            const std::string fileName;
            /// The function that generated the entry.
            const std::string funcName;
            /// The line number that generated the entry.
            const unsigned int lineNum;
            /// The entry's logger's name.
            const std::string loggerName;
            /// The executing file that generated the entry.
            const std::string execFile;

            /// The constructor, is basically just a shortcut to assign all of
            /// the properties.
            LogEntry(std::string message,
                     log_level logLevel,
                     std::string fileName,
                     std::string funcName,
                     unsigned int lineNum,
                     std::string loggerName,
                     std::time_t logTime = time(nullptr),
                     std::string execFile = getExecutingFileName()) :
                         message(std::move(message)),
                         logTime(logTime),
                         logLevel(logLevel),
                         fileName(std::move(fileName)),
                         funcName(std::move(funcName)),
                         lineNum(lineNum),
                         loggerName(std::move(loggerName)),
                         execFile(std::move(execFile)) {}

    };
}

#endif
