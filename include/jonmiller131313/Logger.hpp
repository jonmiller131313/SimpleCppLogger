/**
 * @file Logger.hpp
 * @brief The main logging class.
 * @copyright LGPLv3+ (c) 2019 Jonathan Miller
 * @code{.md}
 ***********************************************************************
 * This file is part of SimpleCppLogger:
 *    https://gitlab.com/jonmiller131313/SimpleCppLogger
 *
 *    SimpleCppLogger is free software: you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public License
 *    as published by the Free Software Foundation, either version 3 of
 *    the License, or (at your option) any later version.
 *
 *    SimpleCppLogger is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with SimpleCppLogger.
 *    If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************
 * @endcode
 */
#ifndef JONMILLER131313_LOGGER_HPP
#define JONMILLER131313_LOGGER_HPP

#include <memory>
#include <mutex>
#include <optional>
#include <string>
#include <utility>
#include <vector>

#include "jonmiller131313/formatters/Formatter.hpp"
#include "jonmiller131313/handlers/Handler.hpp"
#include "jonmiller131313/logging_utils.hpp"

#define _LOGGER_ADD_ENTRY_LEVEL_FUNC(FUNC_NAME, LOG_LEVEL) \
    inline void FUNC_NAME(std::string message, \
                         std::string fileName = __builtin_FILE(), \
                         std::string funcName = __builtin_FUNCTION(), \
                         unsigned int lineNum = __builtin_LINE()) const { \
            addEntry((LOG_LEVEL), std::move(message), std::move(fileName), std::move(funcName), lineNum); \
        }

/// Maintainer: [Jonathan Miller](https://gitlab.com/jonmiller131313)
namespace jonmiller131313 {
/// A simple C++ logging framework, inspired by
/// [Python's `logging` module](https://docs.python.org/3/library/logging.html).
namespace logging {

    /// A simple struct to store parameters for globally configuring all logger objects.
    struct GlobalConfig {
        /// The string format.
        std::string format;
        /// The log level
        log_level level = NOT_SET;
    };

    /// The main logging class.
    class Logger {
        public:
            /**
             * @brief The constructor.
             *
             * @param name  The object's name.
             * @param level The initial log level.
             *
             * @note If called outside of `getLogger()` then the created object will not be found in the global
             *       reference mapping.
             */
            explicit Logger(std::string name, log_level level = NOT_SET);

            /// Cannot have duplicate logging objects.
            Logger(const Logger &cpy) = delete;

            /// No move constructor.
            Logger(Logger &&mvLogger) noexcept;

            /// The destructor
            ~Logger() = default;

            /// No copy assignment operator.
            Logger& operator=(const Logger &cpy) = delete;

            /// no move assignment operator.
            Logger& operator=(Logger &&mvLogger) noexcept;

            /**
             * @brief Gets a logger object.
             *
             * If one does not exist in the global reference list then a new one is created.
             *
             * The log level will be initialized from globalLevel.
             *
             * @param name The logger's name.
             *
             * @return A reference to the logging object.
             */
            [[nodiscard]] static auto getLogger(const std::string &name) -> Logger&;

            /**
             * @brief Gets a logger object.
             *
             * If one does not exist in the global reference list then a new one is created.
             *
             * @param name The logger's name.
             * @param level The logger's level.
             *
             * @return A reference to the logging object.
             */
            [[nodiscard]] static auto getLogger(const std::string &name, log_level level) -> Logger&;

            /**
             * @brief Briefly configures the logger to output to standard out.
             *
             * Does nothing if handlers have already been added.
             *
             * @param fmt The string format.
             * @param level The log level filter.
             *
             * @see StringFormatter::setFormat()
             */
            void basicConfig(std::string fmt, log_level level = NOT_SET);

            /**
             * @brief Cleans up and removes all loggers from Logger::logList.
             */
            static void clearLoggers();

            /**
             * @brief Gets the root logger.
             * @return The root logger
             */
            [[nodiscard]] static auto getRootLogger() -> Logger&;

            /// Alias for getRootLogger()
            [[nodiscard]] static inline auto r() -> Logger& {
                return getRootLogger();
            }

            /**
             * @brief Enables global configuration of all loggers that are created after this call.
             *
             * After calling this function, every logger that's created via Logger::getLogger() will automatically have
             * Logger::basicConfig() called with the same parameters passed in here.
             *
             * This function also calls basicConfig() on the root logging object.
             *
             * @param format The string format.
             * @param level The handler's level
             *
             * @see basicFormat()
             */
            static void globalConfig(std::string format, log_level level = NOT_SET);

            /**
             * @brief Adds a new handler to the logging object.
             *
             * @param handler The new handler.
             */
            template<class H>
            void addHandler(Handler &&handler) {
                auto lock = std::lock_guard(logLock);
                handlers.emplace_back(std::make_unique<H>(std::forward<H>(handler)));
            }

            /**
             * @brief Adds a new handler to the logging object.
             *
             * @param handler The new handler.
             */
            void addHandler(std::unique_ptr<Handler> handlerPtr);

            /**
             * @brief Adds multiple handlers to the logging object.
             *
             * @param newHandlers The list of handlers.
             *
             * @see addHandler()
             */
            void addHandlers(std::vector<std::unique_ptr<Handler>> &&newHandlers);

            /**
             * @brief Sets the log level.
             *
             * @param level The new log level.
             */
            void setLevel(log_level level);

            /**
             * @brief Adds a log entry with a specific level.
             *
             * Must be used when using a custom log level that has been added using @ref registerLogLevel().
             *
             * @param level The log level, if lower than the logger's set level then it is ignored.
             * @param message The log message.
             * @param fileName The name of the file adding the entry. Defaults to the calling function string name.
             * @param funcName The name of the function adding the entry. Defaults to the file name as it was passed
             *                 into the preprocessor.
             * @param lineNum The line number adding the entry. Defaults to the line number of the calling function.
             */
            void addEntry(log_level level,
                          std::string &&message,
                          std::string &&fileName = __builtin_FILE(),
                          std::string &&funcName = __builtin_FUNCTION(),
                          unsigned int lineNum = __builtin_LINE()) const;

            /**
             * @brief A shortcut for addEntry().
             *
             * @see addEntry()
             */
            _LOGGER_ADD_ENTRY_LEVEL_FUNC(fatal, FATAL);

            /**
             * @brief A shortcut for addEntry().
             *
             * @see addEntry()
             */
            _LOGGER_ADD_ENTRY_LEVEL_FUNC(critical, CRITICAL);

            /**
             * @brief A shortcut for addEntry().
             *
             * @see addEntry()
             */
            _LOGGER_ADD_ENTRY_LEVEL_FUNC(error, ERROR);

            /**
             * @brief A shortcut for addEntry().
             *
             * @see addEntry()
             */
            _LOGGER_ADD_ENTRY_LEVEL_FUNC(warning, WARNING);

            /**
             * @brief A shortcut for addEntry().
             *
             * @see addEntry()
             */
            _LOGGER_ADD_ENTRY_LEVEL_FUNC(info, INFO);

            /**
             * @brief A shortcut for addEntry().
             *
             * @see addEntry()
             */
            _LOGGER_ADD_ENTRY_LEVEL_FUNC(verbose, VERBOSE);

            /**
             * @brief A shortcut for addEntry().
             *
             * @see addEntry()
             */
            _LOGGER_ADD_ENTRY_LEVEL_FUNC(debug, DEBUG);

            /**
             * @brief Gets the log level.
             *
             * @return The log level.
             */
            [[nodiscard]] auto getLevel() const -> log_level ;

            /**
             * @brief Gets the logger's name.
             *
             * @return The logger's name.
             */
             [[nodiscard]] auto getName() const -> const std::string&;

        private:
            /// The logging object list, is a pointer due to needing to access it after `main()`.
            static std::unordered_map<std::string, Logger> logList;
            /// The lock for logList.
            static std::mutex logListLock;

            static Logger rootLogger;
            static std::optional<GlobalConfig> config;

            /// The logging object's name.
            std::string name;
            /// The current level filter for the logger.
            log_level logLevel{};
            /// The multithreading lock.
            mutable std::mutex logLock;

            /// The list of handlers.
            std::vector<std::unique_ptr<Handler>> handlers;

    };

}}

#endif
